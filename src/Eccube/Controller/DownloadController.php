<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Controller;

use Eccube\Entity\BaseInfo;
use Eccube\Entity\Master\ProductStatus;
use Eccube\Entity\Product;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Form\Type\AddCartType;
use Eccube\Form\Type\Master\ProductListMaxType;
use Eccube\Form\Type\Master\ProductListOrderByType;
use Eccube\Form\Type\SearchProductType;
use Eccube\Repository\BaseInfoRepository;
use Eccube\Repository\CustomerFavoriteProductRepository;
use Eccube\Repository\Master\ProductListMaxRepository;
use Eccube\Repository\ProductRepository;
use Eccube\Service\CartService;
use Eccube\Service\PurchaseFlow\PurchaseContext;
use Eccube\Service\PurchaseFlow\PurchaseFlow;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DownloadController extends AbstractController
{
    public function __construct(
        //
    ) {
        //
    }

//    /**
//     * @Route("/download", name="download_mp3", methods={"GET"})
//     */
//    public function downloadFileMp3(Request $request)
//    {
//        $youtubeLink = $request->query->get('youtube_link');
//
//        if (!$youtubeLink) {
//            return new JsonResponse([
//                'message' => 'Youtube_link is a required field'
//            ], 400);
//        }
//
//        if (!$this->isValidYoutubeLink($youtubeLink)) {
//            return new JsonResponse([
//                'message' => 'Invalid youTube link'
//            ], 400);
//        }
//
//        $dir = 'video';
//        if (!file_exists($dir)) {
//            mkdir($dir, 0755, true);
//        }
//
//        $dateTime = new \DateTime();
//        $timestamp = $dateTime->getTimestamp();
//        $videoPath = "$dir" . '/video_' . $timestamp . '.mp4';
//        exec("youtube-dl -f 'best[height=360]' -o $videoPath --recode-video mp4 $youtubeLink");
//
//        $mp3Path = "$dir" . '/audio_' . $timestamp .'.mp3';
//        exec("ffmpeg -i $videoPath -vn -ar 44100 -ac 2 -b:a 192k $mp3Path");
//
////        unlink($videoPath);
////        unlink($mp3Path);
//
//        return $this->file($mp3Path);
//    }

    /**
     * @param $youtubeLink
     * @return bool
     */
    public function isValidYoutubeLink($youtubeLink)
    {
        if (strpos($youtubeLink, 'https://www.youtube.com') === 0) {
            return true;
        }

        return false;
    }
}
