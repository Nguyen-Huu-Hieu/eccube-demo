<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

if (!class_exists('\Eccube\Entity\User')) {
    /**
     * TableTest
     *
     * @ORM\Table(name="dtb_user")
     * @ORM\InheritanceType("SINGLE_TABLE")
     * @ORM\DiscriminatorColumn(name="discriminator_type", type="string", length=255)
     * @ORM\HasLifecycleCallbacks()
     * @ORM\Entity(repositoryClass="Eccube\Repository\PageRepository")
     */
    class User extends \Eccube\Entity\AbstractEntity
    {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer", options={"unsigned":true})
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="IDENTITY")
         */
        private $id;

        /**
         * @var string|null
         *
         * @ORM\Column(name="user_name", type="string", length=255, nullable=true)
         */
        private $user_name;

        /**
         * @var string|null
         *
         * @ORM\Column(name="email", type="string", length=255, nullable=true)
         */
        private $email;

        /**
         * @var string|null
         *
         * @ORM\Column(name="gender", type="integer", length=255, nullable=true)
         */
        private $gender;

        /**
         * @var string|null
         *
         * @ORM\Column(name="age", type="string", length=255, nullable=true)
         */
        private $age;

        /**
         * @var string|null
         *
         * @ORM\Column(name="birthday", type="string", length=255, nullable=true)
         */
        private $birthday;

        /**
         * @var string|null
         *
         * @ORM\Column(name="desc", type="string", length=255, nullable=true)
         */
        private $desc;
    }
}
