<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Customize\Controller;

use Eccube\Controller\AbstractController;
use Eccube\Form\Type\DownloadType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DownloadController extends AbstractController
{
    public function __construct(
        //
    ) {
        //
    }

    /**
     * @Route("/download", name="download_mp3", methods={"GET"})
     */
    public function downloadFileMp3(Request $request)
    {
//        $request = $request->request->all();
//        $youtubeLink = $request['youtube_link'];
//        $type = $request['type'];  // MP3: 1, MP4: 2

        $youtubeLink = $request->query->get('youtube_link');
        $type = $request->query->get('type');

        if (!$youtubeLink) {
            return new JsonResponse([
                'message' => 'Youtube_link is a required field'
            ], 400);
        }

        if (!$this->isValidYoutubeLink($youtubeLink)) {
            return new JsonResponse([
                'message' => 'Invalid youTube link'
            ], 400);
        }

        $dir = 'video';
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }

        $dateTime = new \DateTime();
        $timestamp = $dateTime->getTimestamp();
        $videoPath = "$dir" . '/video_' . $timestamp . '.mp4.webm';

        if ($type == 1) {
            $videoPath = "$dir" . '/video_' . $timestamp . '.mp3.webm';
            exec("yt-dlp -f 'bestaudio/best' --audio-format mp3 -o $videoPath $youtubeLink");
        } else if ($type == 2) {
            $videoPath = "$dir" . '/video_' . $timestamp . '.mp4.webm';
            exec("yt-dlp -o $videoPath $youtubeLink");
        }

        return $this->file($videoPath);
    }

    /**
     * @param $youtubeLink
     * @return bool
     */
    public function isValidYoutubeLink($youtubeLink)
    {
        if (strpos($youtubeLink, 'https://www.youtube.com') === 0) {
            return true;
        }

        return false;
    }
}
